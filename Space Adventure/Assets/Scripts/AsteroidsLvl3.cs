﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AsteroidsLvl3 : MonoBehaviour {


    Text text ;
    public static int asteroidHits = 3;
    public AudioClip MusicClip;
    public AudioSource MusicSource;
     


    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        MusicSource.clip = MusicClip;
       
    }
	
	// Update is called once per frame
	 void Update () {
        text.text = asteroidHits.ToString();
        if (asteroidHits== 0)
        {

          
            SceneManager.LoadScene("Level Failed");
        }
    }

   

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            asteroidHits -= 1;
            Destroy(gameObject);
            MusicSource.Play();
        }
    }
}

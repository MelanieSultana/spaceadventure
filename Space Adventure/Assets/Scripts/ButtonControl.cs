﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControl : MonoBehaviour
{
    public AudioClip MusicClip;
    public AudioSource MusicSource;

    // Use this for initialization

    void Start()
    {
        MusicSource.clip = MusicClip;
    }
    public void btnStart()
    {
      
        MusicSource.Play();
        SceneManager.LoadScene("Level1");
        
    }

    // Update is called once per frame
    public void btnSpaceships()
    {
        SceneManager.LoadScene("Shop");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {
   
    public Transform target;

	public Transform bg1;
	public Transform bg2;

	private float size;

    private Vector3 cameraTargetPos = new Vector3();
    private Vector3 bg1TargetPos = new Vector3();
    private Vector3 bg2TargetPos = new Vector3();


    public Transform wallLeft;
    public Transform wallRight;

    public Transform wallLeft1;
    public Transform wallRight1;


    private Vector3 wallLeftTargetPos = new Vector3();
    private Vector3 wallRightTargetPos = new Vector3();

    private Vector3 wallLeftTargetPos1 = new Vector3();
    private Vector3 wallRightTargetPos1 = new Vector3();


    // Use this for initialization
    void Start () {
		//size = bg1.GetComponent<BoxCollider2D> ().size.y;
       // size1 = wallRight.GetComponent<BoxCollider2D>().size.y;

    }

	// Update is called once per frame
	void FixedUpdate () {
		//camera
		Vector3 targetPos = SetPos(cameraTargetPos, target.position.x, target.position.y, transform.position.z);
		transform.position = Vector3.Lerp(transform.position, targetPos,0.2f);

		//background
		if(transform.position.y>= bg2.position.y){

			bg1.position = SetPos(bg1TargetPos, bg1.position.x, bg2.position.y + size, bg1.position.z);
			SwitchBg ();

		}

        if (transform.position.y <= bg1.position.y)
        {

            bg1.position = SetPos(bg2TargetPos, bg2.position.x, bg1.position.y - size, bg2.position.z);
            SwitchBg();

        }

        //colliders
        if (transform.position.y >= wallLeft.position.y)
        {

            wallRight.position = SetPos(wallRightTargetPos, wallRight.position.x, wallLeft.position.y + size, wallRight.position.z);
            wallMove();

        }

        if (transform.position.y <= wallRight.position.y)
        {

            wallRight.position = SetPos(wallLeftTargetPos, wallLeft.position.x, wallRight.position.y - size, wallLeft.position.z);
            wallMove();

        }


        if (transform.position.y >= wallLeft1.position.y)
        {

            wallRight1.position = SetPos(wallRightTargetPos1, wallRight1.position.x, wallLeft1.position.y + size, wallRight1.position.z);
            wallMove1();

        }

        if (transform.position.y <= wallRight1.position.y)
        {

            wallRight1.position = SetPos(wallLeftTargetPos1, wallLeft1.position.x, wallRight1.position.y - size, wallLeft1.position.z);
            wallMove1();

        }
    }

	private void SwitchBg() {
		Transform temp = bg1;
		bg1 = bg2;
		bg2 = temp;

	}

    private void wallMove()
    {
        Transform wall = wallRight;
        wallRight = wallLeft;
        wallLeft = wall;


    }

    private void wallMove1()
    {
        Transform wall1 = wallRight1;
        wallRight1 = wallLeft1;
        wallLeft1 = wall1;


    }

    private Vector3 SetPos(Vector3 pos, float x, float y, float z)
    {

        pos.x = x;
        pos.y = y;
        pos.z = z;
        return pos;

    }
}


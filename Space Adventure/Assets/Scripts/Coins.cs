﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Coins : MonoBehaviour {

    Text text;
    public static int coinAmount = 0;
    public AudioClip MusicClip;
    public AudioSource MusicSource;

    void Start()
    {
        text = GetComponent<Text>();
        MusicSource.clip = MusicClip;
    }

    void Update()
    {
        text.text = coinAmount.ToString();


        if (coinAmount == 10)
        {

            SceneManager.LoadScene("Level2");
        }

        
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player") {
            coinAmount += 1;
            MusicSource.Play();
            Destroy(gameObject);
        }
        
    }

}

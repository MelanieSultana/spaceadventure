﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CoinsLvl3 : MonoBehaviour {

    Text text;
    public static int coinAmount = 0;
    public AudioClip MusicClip;
    public AudioSource MusicSource;

    void Start()
    {
        text = GetComponent<Text>();
        MusicSource.clip = MusicClip;
    }

    void Update()
    {
        text.text = coinAmount.ToString();

        if (coinAmount == 25)
        {

            SceneManager.LoadScene("Win");
        }



    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player") {
            coinAmount += 1;
            MusicSource.Play();
            Destroy(gameObject);
        }
        
    }

}

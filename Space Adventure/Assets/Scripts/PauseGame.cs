﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour
{


    private void Update()
    {
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {

                if (Time.timeScale == 1)
                {
                    Time.timeScale = 0;
                }
                else
                {
                    Time.timeScale = 1;

                }
            }

        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class fueltank : MonoBehaviour
{

    [SerializeField]
    public Text FuelMeter;
    
    private int Fuel = 100;
    public AudioClip MusicClip;
    public AudioSource MusicSource;



    void Start()
    {
        MusicSource.clip = MusicClip;
        //fuelAmount = 100;
        InvokeRepeating("decreaseFuel", 1.0f, 1.0f);
    }

    private IEnumerator decreaseFuel()
    {
        if (Fuel > 0)
        {
            Fuel -= 1;
            yield return new WaitForSeconds(0.01f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
          
            Destroy(gameObject);
            MusicSource.Play();
        }
    }

    void Update()
    {

       // FuelMeter.text = "Fuel: " + Fuel.ToString() + "% ";
        Debug.Log(Fuel);

    }
}

    

    

       






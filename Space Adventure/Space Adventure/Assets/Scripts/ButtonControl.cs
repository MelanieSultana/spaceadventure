﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControl : MonoBehaviour
{

    // Use this for initialization
    public void btnStart()
    {
        SceneManager.LoadScene("Level1");
    }

    // Update is called once per frame
    public void btnSpaceships()
    {
        SceneManager.LoadScene("Shop");
    }
}
